/**
 * @file tll6527m_sysfs_spi.c
 *
 * @brief
 * The file creates a sysfs node in /sys/firmware/tll6527m/spi/ph9device
 *
 * @version 1.0
 * 
 * @date 20121013
 * 
 * @author Karthikeyan GA
 *
 * XqJv Copyright(c) 2010-2012 The Learning Labs,Inc. All Rights Reserved  jVkB
 *
 * Organization: The Learning Labs
 *
 * Tested: Compiler bfin-linux-uclinux-gcc; Output format FDPIC module;
 * Target TLL6527M V1.2
 *
 * 
 * Created: 20120825, Author: Karthikeyan GA, Notes: Created initial version 
 * from the tll6527m_sysfs_spi.c while spiting the logic
 *
 * References:
 *
 * 1. Sample code for creating simple node in sysfs 
 *    http://geekwentfreak.wordpress.com/2010/06/28/
 *    create-files-in-sysfs-sysfs_create_group/
 * 2. The spi summary page from kernel.org 
 *    http://www.kernel.org/doc/Documentation/spi/spi-summary
 * 3. The ADI engineering zone giving a sample code 
 *    http://ez.analog.com/message/58120#58120
 *
 */
#include "tll6527m_sysfs_spi.h"

/**
 * @brief : 
 *  trees is the array of pointers of type struct tll6527m_sysfs_spi_tree
 * to store the reference of the tree_info structures added using the 
 * module 
 */
static struct tll6527m_sysfs_spi_tree *trees[TLL6527M_MAX_SPI_BUS];

/** 
 * @brief
 * create_generic_tree
 *
 * This function creates the generic folder and file nodes and controller_data
 * node with its file nodes to the busnumber folder
 *
 * @param tree_info the pointer of struct tll6527m_sysfs_spi_tree which is
 * partially initiliased by tll6527m_sysfs_spi_tree_init function and is called
 * by it to generate the generic tree
 * @return int 0 on success, others on failure
 */
static int create_generic_tree(struct tll6527m_sysfs_spi_tree *tree_info){
    int retval;
    //create generic node
    tree_info->folders_kobj[1]=kobject_create_and_add(GENERIC_NODE_NAME, \
                                                tree_info->folders_kobj[0]);
    if(!tree_info->folders_kobj[1])
        return ENOMEM;
    //create attr_group for file nodes
    retval = sysfs_create_group(tree_info->folders_kobj[1], \
                    tree_info->file_node_attr_group + 1);
    if(retval){
        kobject_put(tree_info->folders_kobj[1]);
        return -1;
    }
    //create controller node
    tree_info->folders_kobj[2]=kobject_create_and_add \
                    (CONTROLLER_DATA_NODE_NAME,tree_info->folders_kobj[1]);
    if(!tree_info->folders_kobj[2]){
        kobject_put(tree_info->folders_kobj[1]);
        return ENOMEM;
    }
    //Create attr_group for controller folder's files
    retval = sysfs_create_group(tree_info->folders_kobj[2], \
                    tree_info->file_node_attr_group + 2);
    if(retval){
        kobject_put(tree_info->folders_kobj[2]);
        kobject_put(tree_info->folders_kobj[1]);
        return -1;
    }
    return 0;
}

/** remove_generic_tree
 *
 * This function removes the controller_data and the generic folder nodes
 * and their contents. This function can be called during release of the whole
 * tree
 *
 * @param tree_info the pointer of struct tll6527m_sysfs_spi_tree which is
 * initiliased by tll6527m_sysfs_spi_tree_init function 
 *
 * @return int 0 on success, others on failure
 */
static int remove_generic_tree(struct tll6527m_sysfs_spi_tree *tree_info){
    //remove controller_data node
    kobject_put(tree_info->folders_kobj[2]);
    //then remove the generic node
    kobject_put(tree_info->folders_kobj[1]);
    return 0;
}


/** show_device
 *
 * This function prints the currently loaded device. This funcition just prints
 * the modalias_name. This function is the function that needs to be passed as 
 * the function pointer to the show of kobj_attribute structure.
 *
 * @param kobj the pointer of kobject on which the read is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @return int the size of the buf 
 */
static ssize_t show_device(  \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             char * buf)
{
    //Get the spi bus number
    char *residue;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    //print the value of modalias_name which has the current loaded device
    return sprintf(buf, "%s\n", trees[spi_bus_number]->current_modalias_name);
}

/** load_new_device
 *
 *  This function checks if any previous device has to be unregistered. If so it
 * unregisters the spi device. Then the function scans the array containing the 
 * board info of each supported device.
 * if the device is found then particular device is loaded to the spi. If the
 * device is not found then it issues a device not found info message and 
 * returns.
 *
 * @param kobj the pointer of kobject on which the write is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @param count the length of buf which is written to the node
 * @return int the size of the buf 
 */
static ssize_t load_new_device( \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             const char * buf, \
                             size_t count){
     int i;
     int num_devices;
     struct spi_device *current_device;
     char *modalias_name;
     struct spi_master *master;
     //Get the spi bus number
     char *residue;
     int spi_bus_number;
     static struct spi_board_info *tll_hotswap_spi_devices_board_info;
     spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
     if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
     }
     current_device=trees[spi_bus_number]->current_device;
     modalias_name=trees[spi_bus_number]->current_modalias_name;
     tll_hotswap_spi_devices_board_info=trees[spi_bus_number]->hot_swap_device_list;
     num_devices=trees[spi_bus_number]->hot_swap_devices_count;
     //if same device is asked to be set then do nothing
     if(current_device!=NULL &&    \
              strncmp(modalias_name,buf,SPI_NAME_SIZE) == 0 ){
         return count;
     }
     //Copy the current device name
     strncpy(modalias_name,buf,SPI_NAME_SIZE);
     //The buf will have a \n char at the end so need to remove it
     modalias_name[strlen(modalias_name)-1]=0;
     //Unregister device if some device was already loaded
     if(current_device!=NULL){
         spi_unregister_device(current_device);
         trees[spi_bus_number]->current_device=NULL;
     }
     //Special case for disable and generic device
     if(strncmp("disable",modalias_name,SPI_NAME_SIZE) == 0){
         printk(KERN_INFO "%s is currently disabled\n", \
               trees[spi_bus_number]->device_node_name);
         return count;
     }
     else if(strncmp("generic",modalias_name,SPI_NAME_SIZE) == 0){
         trees[spi_bus_number]->generic_device_info.bus_num = spi_bus_number;
         trees[spi_bus_number]->generic_device_info.controller_data= \
              &(trees[spi_bus_number]->controller_data);
         trees[spi_bus_number]->generic_device_info.platform_data = NULL;
         //get the spi master device reference by bus number
         master= spi_busnum_to_master(spi_bus_number);
         //create new device
         trees[spi_bus_number]->current_device=spi_new_device(master,  \
             &(trees[spi_bus_number]->generic_device_info));
         printk(KERN_INFO "%s is in generic device mode\n", \
               trees[spi_bus_number]->device_node_name);
         return count;
     }
     
     /*
      * loop through the tll_hotswap_spi_devices_board_info array to find the 
      * details of the requested device
      */
     for (i=0;i<num_devices;i++){
         //The given name should be matched with modalias name in the board info
         if(strncmp(tll_hotswap_spi_devices_board_info[i].modalias,   \
               modalias_name,SPI_NAME_SIZE) == 0){
             //get the spi master device reference by bus number
             master= spi_busnum_to_master( \
                            tll_hotswap_spi_devices_board_info[i].bus_num);
             //create new device
             trees[spi_bus_number]->current_device=spi_new_device(master,  \
                            &(tll_hotswap_spi_devices_board_info[i]));
             printk(KERN_INFO "%s successfully loaded\n",modalias_name);
             return count;
         }
     }
     
     printk(KERN_ALERT "%s device is not supported or not a valid chip\n", \
                   modalias_name);
     //The modalias is made empty to reflect that no devices are loaded
     strncpy(modalias_name,"disable",SPI_NAME_SIZE);
     return count;
}

/** show_chip_select
 *
 * This function prints the current chip select value of the generic device
 *
 * @param kobj the pointer of kobject on which the read is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @return int the size of the buf 
 */
static ssize_t show_chip_select(  \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             char * buf)
{
    //Get the spi bus number
    char *residue;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    //print the value of chip select
    return sprintf(buf, "%d\n", \
           trees[spi_bus_number]->generic_device_info.chip_select);
}

/** change_chip_select
 *
 *  This function changes the chip select of generic spi device 
 * after checking if the cs is within the allowed values
 *
 * @param kobj the pointer of kobject on which the write is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @param count the length of buf which is written to the node
 * @return int the size of the buf 
 */
static ssize_t change_chip_select( \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             const char * buf, \
                             size_t count){
    char cs_str[SPI_NAME_SIZE];
    int cs;
    int i;
    //Get the spi bus number
    char *residue;
    struct tll6527m_sysfs_spi_tree *tree_info;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    tree_info=trees[spi_bus_number];
    if (strncmp(tree_info->current_modalias_name,"disable",SPI_NAME_SIZE)!=0){
        printk(KERN_ALERT "Disable the device before changing any settings");
        return -1;
    }
    //Copy the current device name
    strncpy(cs_str,buf,SPI_NAME_SIZE);
    //The buf will have a \n char at the end so need to remove it
    cs_str[strlen(cs_str)-1]=0;
    cs=(int)simple_strtoll(cs_str,&residue,10);
    for(i=0;i<tree_info->allowed_cs_values_count;i++){
        if(tree_info->allowed_cs_values[i]==cs){
            tree_info->generic_device_info.chip_select=cs;
            return count;
        }
    }
    printk(KERN_ALERT "%d is not a valid cs value.\n",cs);
    return -1;
}

/** show_modalias
 *
 * This function prints the current modalias value of the generic device
 *
 * @param kobj the pointer of kobject on which the read is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @return int the size of the buf 
 */
static ssize_t show_modalias(  \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             char * buf)
{
    //Get the spi bus number
    char *residue;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    //print the value of modalias 
    return sprintf(buf, "%s\n", \
           trees[spi_bus_number]->generic_device_info.modalias);
}

/** change_modalias
 *
 *  This function changes the modalias of generic spi device 
 *
 * @param kobj the pointer of kobject on which the write is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @param count the length of buf which is written to the node
 * @return int the size of the buf 
 */
static ssize_t change_modalias( \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             const char * buf, \
                             size_t count){
    //Get the spi bus number
    char *residue;
    struct tll6527m_sysfs_spi_tree *tree_info;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    tree_info=trees[spi_bus_number];
    if (strncmp(tree_info->current_modalias_name,"disable",SPI_NAME_SIZE)!=0){
        printk(KERN_ALERT "Disable the device before changing any settings");
        return -1;
    }
    //Copy the current device name
    strncpy(tree_info->generic_device_info.modalias,buf,SPI_NAME_SIZE);
    //The buf will have a \n char at the end so need to remove it
    tree_info->generic_device_info.modalias \
           [strlen(tree_info->generic_device_info.modalias)-1]=0;
    return count;
}

/** show_max_speed_hz
 *
 * This function prints the current max_speed_hz value of the generic device
 *
 * @param kobj the pointer of kobject on which the read is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @return int the size of the buf 
 */
static ssize_t show_max_speed_hz(  \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             char * buf)
{
    //Get the spi bus number
    char *residue;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    //print the value of max_speed_hz 
    return sprintf(buf, "%d\n", \
           trees[spi_bus_number]->generic_device_info.max_speed_hz);
}

/** change_max_speed_hz
 *
 *  This function changes the max_speed_hz of generic spi device
 *
 * @param kobj the pointer of kobject on which the write is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @param count the length of buf which is written to the node
 * @return int the size of the buf 
 */
static ssize_t change_max_speed_hz( \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             const char * buf, \
                             size_t count){
    char temp_str[SPI_NAME_SIZE];
    //Get the spi bus number
    char *residue;
    struct tll6527m_sysfs_spi_tree *tree_info;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_INFO "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    tree_info=trees[spi_bus_number];
    if (strncmp(tree_info->current_modalias_name,"disable",SPI_NAME_SIZE)!=0){
        printk(KERN_ALERT "Disable the device before changing any settings");
        return -1;
    }
    //Copy the current device name
    strncpy(temp_str,buf,SPI_NAME_SIZE);
    //The buf will have a \n char at the end so need to remove it
    temp_str[strlen(temp_str)-1]=0;
    tree_info->generic_device_info.max_speed_hz= \
                   (int)simple_strtoll(temp_str,&residue,10);
    return count;
}

/** show_spi_mode
 *
 * This function prints the current spi_mode value of the generic device
 *
 * @param kobj the pointer of kobject on which the read is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @return int the size of the buf 
 */
static ssize_t show_spi_mode(  \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             char * buf)
{
    //Get the spi bus number
    char *residue;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    //print the value of spi_mode 
    return sprintf(buf, "%d\n", \
           trees[spi_bus_number]->generic_device_info.mode);
}

/** change_spi_mode
 *
 *  This function changes the mode of generic spi device
 *
 * @param kobj the pointer of kobject on which the write is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @param count the length of buf which is written to the node
 * @return int the size of the buf 
 */
static ssize_t change_spi_mode( \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             const char * buf, \
                             size_t count){
    char temp_str[SPI_NAME_SIZE];
    int spi_mode;
    //Get the spi bus number
    char *residue;
    struct tll6527m_sysfs_spi_tree *tree_info;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_INFO "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    tree_info=trees[spi_bus_number];
    if (strncmp(tree_info->current_modalias_name,"disable",SPI_NAME_SIZE)!=0){
        printk(KERN_ALERT "Disable the device before changing any settings");
        return -1;
    }
    //Copy the current device name
    strncpy(temp_str,buf,SPI_NAME_SIZE);
    //The buf will have a \n char at the end so need to remove it
    temp_str[strlen(temp_str)-1]=0;
    spi_mode=(int)simple_strtoll(temp_str,&residue,10);
    if (spi_mode<0 || spi_mode >3){
        printk(KERN_ALERT "invalid spi mode - %d\n",spi_mode);
        return -1;
    }
    tree_info->generic_device_info.mode=spi_mode;
    return count;
}

/** show_ctl_reg
 *
 * This function prints the current ctl_reg value of the controller data
 * of the generic device
 *
 * @param kobj the pointer of kobject on which the read is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @return int the size of the buf 
 */
static ssize_t show_ctl_reg(  \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             char * buf)
{
    //Get the spi bus number
    char *residue;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    //print the value of ctl_reg 
    return sprintf(buf, "%d\n", \
           trees[spi_bus_number]->controller_data.ctl_reg);
}

/** change_ctl_reg
 *
 *  This function changes the ctl_reg value of the controller data
 * of the generic device
 *
 * @param kobj the pointer of kobject on which the write is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @param count the length of buf which is written to the node
 * @return int the size of the buf 
 */
static ssize_t change_ctl_reg( \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             const char * buf, \
                             size_t count){
    char temp_str[SPI_NAME_SIZE];
    //Get the spi bus number
    char *residue;
    struct tll6527m_sysfs_spi_tree *tree_info;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    tree_info=trees[spi_bus_number];
    if (strncmp(tree_info->current_modalias_name,"disable",SPI_NAME_SIZE)!=0){
        printk(KERN_ALERT "Disable the device before changing any settings");
        return -1;
    }
    //Copy the current device name
    strncpy(temp_str,buf,SPI_NAME_SIZE);
    //The buf will have a \n char at the end so need to remove it
    temp_str[strlen(temp_str)-1]=0;
    tree_info->controller_data.ctl_reg= \
                   (u16)simple_strtoll(temp_str,&residue,10);
    return count;
}

/** show_enable_dma
 *
 * This function prints the current enable_dma value of the controller data
 * of the generic device
 *
 * @param kobj the pointer of kobject on which the read is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @return int the size of the buf 
 */
static ssize_t show_enable_dma(  \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             char * buf)
{
    //Get the spi bus number
    char *residue;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    //print the value of enable_dma 
    return sprintf(buf, "%d\n", \
           trees[spi_bus_number]->controller_data.enable_dma);
}

/** change_enable_dma
 *
 *  This function changes the enable_dma value of the controller data
 * of the generic device
 *
 * @param kobj the pointer of kobject on which the write is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @param count the length of buf which is written to the node
 * @return int the size of the buf 
 */
static ssize_t change_enable_dma( \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             const char * buf, \
                             size_t count){
    char temp_str[SPI_NAME_SIZE];
    //Get the spi bus number
    char *residue;
    struct tll6527m_sysfs_spi_tree *tree_info;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    tree_info=trees[spi_bus_number];
    if (strncmp(tree_info->current_modalias_name,"disable",SPI_NAME_SIZE)!=0){
        printk(KERN_ALERT "Disable the device before changing any settings");
        return -1;
    }
    //Copy the current device name
    strncpy(temp_str,buf,SPI_NAME_SIZE);
    //The buf will have a \n char at the end so need to remove it
    temp_str[strlen(temp_str)-1]=0;
    tree_info->controller_data.enable_dma= \
                   (u8)simple_strtoll(temp_str,&residue,10);
    return count;
}

/** show_cs_chg_udelay
 *
 * This function prints the current cs_chg_udelay value of the controller data
 * of the generic device
 *
 * @param kobj the pointer of kobject on which the read is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @return int the size of the buf 
 */
static ssize_t show_cs_chg_udelay(  \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             char * buf)
{
    //Get the spi bus number
    char *residue;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    //print the value of cs_chg_udelay 
    return sprintf(buf, "%d\n", \
           trees[spi_bus_number]->controller_data.cs_chg_udelay);
}

/** change_cs_chg_udelay
 *
 *  This function changes the cs_chg_udelay value of the controller data
 * of the generic device
 *
 * @param kobj the pointer of kobject on which the write is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @param count the length of buf which is written to the node
 * @return int the size of the buf 
 */
static ssize_t change_cs_chg_udelay( \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             const char * buf, \
                             size_t count){
    char temp_str[SPI_NAME_SIZE];
    //Get the spi bus number
    char *residue;
    struct tll6527m_sysfs_spi_tree *tree_info;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    tree_info=trees[spi_bus_number];
    if (strncmp(tree_info->current_modalias_name,"disable",SPI_NAME_SIZE)!=0){
        printk(KERN_ALERT "Disable the device before changing any settings");
        return -1;
    }
    //Copy the current device name
    strncpy(temp_str,buf,SPI_NAME_SIZE);
    //The buf will have a \n char at the end so need to remove it
    temp_str[strlen(temp_str)-1]=0;
    tree_info->controller_data.cs_chg_udelay= \
                   (u16)simple_strtoll(temp_str,&residue,10);
    return count;
}

/** show_idle_tx_val
 *
 * This function prints the current idle_tx_val value of the controller data
 * of the generic device
 *
 * @param kobj the pointer of kobject on which the read is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @return int the size of the buf 
 */
static ssize_t show_idle_tx_val(  \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             char * buf)
{
    //Get the spi bus number
    char *residue;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    //print the value of idle_tx_val 
    return sprintf(buf, "%d\n", \
           trees[spi_bus_number]->controller_data.idle_tx_val);
}

/** change_idle_tx_val
 *
 *  This function changes the idle_tx_val value of the controller data
 * of the generic device
 *
 * @param kobj the pointer of kobject on which the write is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @param count the length of buf which is written to the node
 * @return int the size of the buf 
 */
static ssize_t change_idle_tx_val( \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             const char * buf, \
                             size_t count){
    char temp_str[SPI_NAME_SIZE];
    //Get the spi bus number
    char *residue;
    struct tll6527m_sysfs_spi_tree *tree_info;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    tree_info=trees[spi_bus_number];
    if (strncmp(tree_info->current_modalias_name,"disable",SPI_NAME_SIZE)!=0){
        printk(KERN_ALERT "Disable the device before changing any settings");
        return -1;
    }
    //Copy the current device name
    strncpy(temp_str,buf,SPI_NAME_SIZE);
    //The buf will have a \n char at the end so need to remove it
    temp_str[strlen(temp_str)-1]=0;
    tree_info->controller_data.idle_tx_val= \
                   (u16)simple_strtoll(temp_str,&residue,10);
    return count;
}

/** show_pio_interrupt
 *
 * This function prints the current pio_interrupt value of the controller data
 * of the generic device
 *
 * @param kobj the pointer of kobject on which the read is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @return int the size of the buf 
 */
static ssize_t show_pio_interrupt(  \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             char * buf)
{
    //Get the spi bus number
    char *residue;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    //print the value of pio_interrupt 
    return sprintf(buf, "%d\n", \
           trees[spi_bus_number]->controller_data.pio_interrupt);
}

/** change_pio_interrupt
 *
 *  This function changes the pio_interrupt value of the controller data
 * of the generic device
 *
 * @param kobj the pointer of kobject on which the write is done
 * @param attr the pointer of kobj_attribute on which the read is done
 * @param buf  the buffer to which the value of the contents when a read on the
 *             the node is issued
 * @param count the length of buf which is written to the node
 * @return int the size of the buf 
 */
static ssize_t change_pio_interrupt( \
                             struct kobject * kobj, \
                             struct kobj_attribute * attr, \
                             const char * buf, \
                             size_t count){
    char temp_str[SPI_NAME_SIZE];
    //Get the spi bus number
    char *residue;
    struct tll6527m_sysfs_spi_tree *tree_info;
    int spi_bus_number;
    spi_bus_number=(int)simple_strtoll(kobj->name,&residue,10);
    if (spi_bus_number >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n",spi_bus_number);
        return -1;
    }
    tree_info=trees[spi_bus_number];
    if (strncmp(tree_info->current_modalias_name,"disable",SPI_NAME_SIZE)!=0){
        printk(KERN_ALERT "Disable the device before changing any settings");
        return -1;
    }
    //Copy the current device name
    strncpy(temp_str,buf,SPI_NAME_SIZE);
    //The buf will have a \n char at the end so need to remove it
    temp_str[strlen(temp_str)-1]=0;
    tree_info->controller_data.pio_interrupt= \
                   (u8)simple_strtoll(temp_str,&residue,10);
    return count;
}

/** 
 * @brief
 * tll6527m_sysfs_spi_tree_init
 *
 *  This function initialises the different kobject related members
 * in the struct tll6527m_sysfs_spi_tree for creating the nodes. Then it creates
 * the nodes and returns 0
 *
 * @param tree_info the pointer of tll6527m_sysfs_spi_tree which has to be used
 * for creating the tree folder structure
 * @param parent_kobj the pointer of kobject of the parent node under which the 
 * tree has to be created
 * @return int 0 on success, others on failure
 */
int tll6527m_sysfs_spi_tree_init(struct tll6527m_sysfs_spi_tree *tree_info,
                                 struct kobject *parent_kobj) {
    char spi_bus_num_str[20];
    int retval;
    //initiliaise members in tree struct
    tree_info->current_device=NULL;
    //Assigns the functions for read and write for different nodes
    //file nodes inside <bus-number> folder node
    __ATTR_REDEF_STR(tree_info->bus_num_nodes[0], tree_info->device_node_name, \
                             0666, show_device, load_new_device)
    //file nodes inside generic folder node
    __ATTR_REDEF(tree_info->generic_nodes[0], CHIP_SELECT_NODE_NAME, \
                             0666, show_chip_select, change_chip_select)
    __ATTR_REDEF(tree_info->generic_nodes[1], MODALIAS_NODE_NAME, \
                             0666, show_modalias, change_modalias)
    __ATTR_REDEF(tree_info->generic_nodes[2], MAX_SPEED_HZ_NODE_NAME, \
                             0666, show_max_speed_hz, change_max_speed_hz)
    __ATTR_REDEF(tree_info->generic_nodes[3], SPI_MODE_NODE_NAME, \
                             0666, show_spi_mode, change_spi_mode)
    //file nodes inside controller_data folder node
    __ATTR_REDEF(tree_info->controller_data_nodes[0], CTL_REG_NODE_NAME, \
                             0666, show_ctl_reg, change_ctl_reg)
    __ATTR_REDEF(tree_info->controller_data_nodes[1], ENABLE_DMA_NODE_NAME, \
                             0666, show_enable_dma, change_enable_dma)
    __ATTR_REDEF(tree_info->controller_data_nodes[2],CS_CHG_UDELAY_NODE_NAME, \
                             0666, show_cs_chg_udelay, change_cs_chg_udelay)
    __ATTR_REDEF(tree_info->controller_data_nodes[3], IDLE_TX_VAL_NODE_NAME, \
                             0666, show_idle_tx_val, change_idle_tx_val)
    __ATTR_REDEF(tree_info->controller_data_nodes[4], PIO_INTERRUPT_NODE_NAME, \
                             0666, show_pio_interrupt, change_pio_interrupt)
    //Assigns the attr of the kobj_attribute to the attr of arrtibute
    //file nodes in bus_number folder node
    tree_info->bus_num_node_attr[0]=&(tree_info->bus_num_nodes[0].attr);
    tree_info->bus_num_node_attr[1]=NULL;
    //file nodes in generic folder node
    tree_info->generic_node_attr[0]=&(tree_info->generic_nodes[0].attr);
    tree_info->generic_node_attr[1]=&(tree_info->generic_nodes[1].attr);
    tree_info->generic_node_attr[2]=&(tree_info->generic_nodes[2].attr);
    tree_info->generic_node_attr[3]=&(tree_info->generic_nodes[3].attr);
    tree_info->generic_node_attr[4]=NULL;
    //file nodes in controller_data folder node
    tree_info->controller_data_node_attr[0]= \
              &(tree_info->controller_data_nodes[0].attr);
    tree_info->controller_data_node_attr[1]= \
              &(tree_info->controller_data_nodes[1].attr);
    tree_info->controller_data_node_attr[2]= \
              &(tree_info->controller_data_nodes[2].attr);
    tree_info->controller_data_node_attr[3]= \
              &(tree_info->controller_data_nodes[3].attr);
    tree_info->controller_data_node_attr[4]= \
              &(tree_info->controller_data_nodes[4].attr);
    tree_info->controller_data_node_attr[5]=NULL;
    //Assigning the attrs to attr group
    //nodes in <bus-number> folder node
    tree_info->file_node_attr_group[0].attrs = tree_info->bus_num_node_attr;
    //nodes in generic folder node
    tree_info->file_node_attr_group[1].attrs = tree_info->generic_node_attr;
    //nodes in controller_data node
    tree_info->file_node_attr_group[2].attrs = \
              tree_info->controller_data_node_attr;
    //assign the tree info to the trees array
    if (tree_info->spi_bus_num >= TLL6527M_MAX_SPI_BUS ){
        printk(KERN_ALERT "invalid spi bus number - %d\n", \
              tree_info->spi_bus_num);
        return -1;
    }
    trees[tree_info->spi_bus_num]=tree_info;
    //create spi_bus_num node as given in the treeinfo
    sprintf(spi_bus_num_str, "%d",tree_info->spi_bus_num);
    tree_info->folders_kobj[0]=kobject_create_and_add(spi_bus_num_str, \
                                                parent_kobj);
    if(!tree_info->folders_kobj[0])
        return ENOMEM;
    //create device name
    retval = sysfs_create_group(tree_info->folders_kobj[0], \
                    tree_info->file_node_attr_group);
    if(retval){
        kobject_put(tree_info->folders_kobj[0]);
        return -1;
    }
    //create generic folder node with its sub files and folders
    retval = create_generic_tree(tree_info);
    if(retval){
	kobject_put(tree_info->folders_kobj[0]);
	return -1;
    }
    return 0;
}                                 


/** 
 * @brief
 * tll6527m_sysfs_spi_tree_release
 *  This function removes the generic foder structure and the bus number
 * folder node denoted by the tree_info
 *
 * @param tree_info the pointer of tll6527m_sysfs_spi_tree which has to be used
 * for removing the tree folder structure
 * @return int 0 on success and others on failure
 */
int tll6527m_sysfs_spi_tree_release \
                      (struct tll6527m_sysfs_spi_tree *tree_info){
    //remove the generic folder and its contents
    remove_generic_tree(tree_info);
    //remove the bus number folder and its contents
    kobject_put(tree_info->folders_kobj[0]);
    return 0;
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Karthikeyan GA");

