
/**
 * @file tll6527m_sysfs_spi_main.c
 *
 * @brief
 * The file creates a sysfs node in /sys/firmware/tll6527m/spi/ph9device
 *
 * @version 1.3
 * 
 * @date 20121013
 * 
 * @author Karthikeyan GA
 *
 * XqJv Copyright(c) 2010-2012 The Learning Labs,Inc. All Rights Reserved  jVkB
 *
 * Organization: The Learning Labs
 *
 * Tested: Compiler bfin-linux-uclinux-gcc; Output format FDPIC module;
 * Target TLL6527M V1.2
 *
 * Revised : 20121013, Author: Karthikeyan GA, Notes: Split the module into 2
 * files
 *
 * Revised : 20121012, Author: Karthikeyan GA, Notes: changed the module to a 
 * framework so that other generic devices can be loaded.
 *
 * Revised : 20121010, Author: Karthikeyan GA, Notes: Added the spi board info
 * in the tll_hotswap_spi_devices_board_info array for the custom driver
 * for timer_spi_adc
 * 
 * Created: 20120825, Author: Karthikeyan GA, Notes: Created initial version
 *
 * References:
 *
 * 1. Sample code for creating simple node in sysfs 
 *    http://geekwentfreak.wordpress.com/2010/06/28/
 *    create-files-in-sysfs-sysfs_create_group/
 * 2. The spi summary page from kernel.org 
 *    http://www.kernel.org/doc/Documentation/spi/spi-summary
 * 3. The ADI engineering zone giving a sample code 
 *    http://ez.analog.com/message/58120#58120
 *
 */

#include "tll6527m_sysfs_spi.h"

/**
 * @brief : 
 *  spidev_chip_info is the bfin5xx_spi_chip structure which has the details 
 * about the controller data that has to go to the board_info for spidev
 */
static struct bfin5xx_spi_chip spidev_chip_info = {
	.enable_dma = 0,
//	.bits_per_word = 8,
//removed from structure bfin5xx_spi_chip
};

/**
 * @brief : 
 *  spidev_chip_info is the bfin5xx_spi_chip structure which has the details 
 * about the controller data that has to go to the board_info for ad7476/ad7495
 */
static struct bfin5xx_spi_chip  ad7476_chip_info = {
	.enable_dma = 0,
};

/**
 * @brief : 
 *  spidev_chip_info is the bfin5xx_spi_chip structure which has the details 
 * about the platform data (VREF) that has to go to the board_info for ad7476
 */
static struct ad7476_platform_data ad7476_pdata = {
	.vref_mv = 3300,
};

/**
 * @brief : 
 *  tll_hotswap_spi_devices_board_info is an array of spi_board_info which has
 * the list of supported devices that are loaded using
 */
static struct spi_board_info tll_hotswap_spi_devices_board_info_bus0[] = {
	/* Settings for spidev*/
	{
		.modalias = "spidev",
		.max_speed_hz = 20000000,
		/* TLL6527Mv1-0 supports max spi clock (SCK) speed = 10 MHz */
		.bus_num = 0,
		.chip_select = GPIO_PH9 + MAX_CTRL_CS,
		.mode = SPI_MODE_3,
		.controller_data = &spidev_chip_info,
	},
	/* Settings for AD7476 device*/
	{
		/* the modalias must be the same as spi device driver name */
		.modalias = "ad7476", /* Name of spi_driver for this device */
		.max_speed_hz = 1000000, /* max spi clock (SCK) speed in HZ */
		.bus_num = 0, /* Framework bus number */
		.chip_select = GPIO_PH9 + MAX_CTRL_CS,
/* Can't use native SPI CS because of bug:
 * http://blackfin.uclinux.org/gf/project/uclinux-dist/forum/?_forum_action=
 * MessageReply&message_id=102381&action=ForumBrowse
 */
		/*.chip_select = 5,*/ /* Framework chip select */
		.platform_data = &ad7476_pdata,
		.controller_data = &ad7476_chip_info, /* Blackfin only */
		.mode = SPI_MODE_0,
	},
	/* Settings for AD7495 device*/
	{
		/* the modalias must be the same as spi device driver name */
		.modalias = "ad7495", /* Name of spi_driver for this device */
		.max_speed_hz = 1000000, /* max spi clock (SCK) speed in HZ */
		.bus_num = 0, /* Framework bus number */
		.chip_select = GPIO_PH9 + MAX_CTRL_CS,
/* Can't use native SPI CS because of bug:
 * http://blackfin.uclinux.org/gf/project/uclinux-dist/forum/?_forum_action=
 * MessageReply&message_id=102381&action=ForumBrowse
 */
		/*.chip_select = 5,*/ /* Framework chip select */
		//.platform_data = &ad7476_pdata,
		.platform_data = NULL,
		.controller_data = &ad7476_chip_info, /* Blackfin only */
		.mode = SPI_MODE_0,
	},
	/* Settings for timer_spi_adc*/
	{
		.modalias = "timer_spi_adc",
		.max_speed_hz = 1000000,
		/* TLL6527Mv1-0 supports max spi clock (SCK) speed = 10 MHz */
		.bus_num = 0,
		.chip_select = GPIO_PH9 + MAX_CTRL_CS,
		.mode = SPI_MODE_0,
		.controller_data = &spidev_chip_info,
	}
};

/**
 * @brief : 
 *  allowed_cs_values_bus_0 is an array of int which has
 * the list of allowed values of chip select for PH9
 */
static int allowed_cs_values_bus_0[]={5,GPIO_PH9 + MAX_CTRL_CS};
/**
 * @brief : 
 *  bus_0_tree is a structure of type tll6527m_sysfs_spi_tree which has
 * the list of configuration required to crete the <bus-number> tree and
 * it configures the default devic as disable and the default settings for the
 * generic device as the spidev device
 */
static struct tll6527m_sysfs_spi_tree bus_0_tree={
    .spi_bus_num=0,
    .current_modalias_name="disable",
    .device_node_name=PH9_NODE,
    .hot_swap_device_list=tll_hotswap_spi_devices_board_info_bus0,
    .hot_swap_devices_count=ARRAY_SIZE(tll_hotswap_spi_devices_board_info_bus0),
    .allowed_cs_values=allowed_cs_values_bus_0,
    .allowed_cs_values_count=ARRAY_SIZE(allowed_cs_values_bus_0),
    .generic_device_info= {
		.modalias = "spidev",
		.max_speed_hz = 20000000,
		/* TLL6527Mv1-0 supports max spi clock (SCK) speed = 10 MHz */
		.bus_num = 0,
		.chip_select = GPIO_PH9 + MAX_CTRL_CS,
		.mode = SPI_MODE_3,
     },
    .controller_data = {
	         .enable_dma = 0,
     },
};

/**
 * @brief : 
 *  tll6527m_spi_sysfs_kobj is the kobject to store the reference of the 
 * /sys/firmware/spi node
 */
static struct kobject *tll6527m_spi_sysfs_kobj;


/** tll6527m_sysfs_init
 *
 *  This function creates the /sys/firmware/spi node and its content depending
 * on the defintion of bus_0_tree
 *
 * @return int the return value of sysfs and in case of error return the error
 *             number
 */
static int __init tll6527m_sysfs_init(void)
{
 int retval;
 //firmware_kobj is the global reference of the kobject of /sys/firmware node
 tll6527m_spi_sysfs_kobj= \
             kobject_create_and_add(TLL6527M_SPI_NODE, firmware_kobj);
 if(!tll6527m_spi_sysfs_kobj)
     return ENOMEM;
 //Create the 0th bus tree
 retval = tll6527m_sysfs_spi_tree_init(&bus_0_tree,tll6527m_spi_sysfs_kobj);
 if(retval)
     kobject_put(tll6527m_spi_sysfs_kobj);
 printk(KERN_INFO "tll6527m_sysfs_spi has been loaded\n");
 return retval;
}



/** tll6527m_sysfs_exit
 *
 *  This function decrements the reference of the parent nodes by one using
 * kobject_put
 * @return int the size of the buf 
 */
static void __exit tll6527m_sysfs_exit(void)
{
     tll6527m_sysfs_spi_tree_release(&bus_0_tree);
     kobject_put(tll6527m_spi_sysfs_kobj);
     printk(KERN_INFO "tll6527m_sysfs_spi has been unloaded\n");
}

//the function that initialises the module
module_init(tll6527m_sysfs_init);
//the function the removes the module
module_exit(tll6527m_sysfs_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Karthikeyan GA");
