/* File:	arch/blackfin/mach-bf527/boards/tll6527m.c
 * Based on:	arch/blackfin/mach-bf527/boards/ezkit.c
 * Author:	Ashish Gupta,KA
 *
 * Copyright: 2011 - The Learning Labs Inc.
 *
 * Licensed under the GPL-2 or later.
 */

#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/physmap.h>
#include <linux/spi/spi.h>
#include <linux/spi/flash.h>
#include <linux/i2c.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/usb/musb.h>
#include <linux/leds.h>
#include <linux/input.h>
#include <asm/dma.h>
#include <asm/bfin5xx_spi.h>
#include <asm/reboot.h>
#include <asm/nand.h>
#include <asm/portmux.h>
#include <asm/dpmc.h>
#include <linux/i2c/adp5588.h>
#include <asm/bfin_sport.h>
#include <../drivers/staging/iio/iio.h>
#include <../drivers/staging/iio/adc/ad7476.h>

#if defined(CONFIG_TOUCHSCREEN_AD7879) \
	|| defined(CONFIG_TOUCHSCREEN_AD7879_MODULE)
#include <linux/spi/ad7879.h>
#define LCD_BACKLIGHT_GPIO 0x50
/* TLL6527M uses TLL7UIQ35 user interface module. AD7879 AUX GPIO is used for
 * LCD Backlight Enable
 */
#endif

/*
 * Name the Board for the /proc/cpuinfo
 */
const char bfin_board_name[] = "TLL6527Mv1-1";
/*
 *  Driver needs to know address, irq and flag pin.
 */

#if defined(CONFIG_USB_MUSB_HDRC) || defined(CONFIG_USB_MUSB_HDRC_MODULE)
static struct resource musb_resources[] = {
	[0] = {
		.start	= 0xffc03800,
		.end	= 0xffc03cff,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {	/* general IRQ */
		.start	= IRQ_USB_INT0,
		.end	= IRQ_USB_INT0,
		.flags	= IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.name	= "mc"
	},
	[2] = {	/* DMA IRQ */
		.start	= IRQ_USB_DMA,
		.end	= IRQ_USB_DMA,
		.flags	= IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.name	= "dma"
	},
};

static struct musb_hdrc_config musb_config = {
	.multipoint	= 0,
	.dyn_fifo	= 0,
	.soft_con	= 1,
	.high_iso_tx	= 1,
	.high_iso_rx	= 1,
	.dma		= 1,
	.num_eps	= 8,
	.dma_channels	= 8,
	.gpio_vrsel	= GPIO_PG11, 
	/* tll6527mv1-1 uses GPIO PG11 for ltc3576 5V EN_OTG */

	/* Some custom boards need to be active low, just set it to "0"
	 * if it is the case.
	 */
	.gpio_vrsel_active	= 1,
	.clkin		= 24,	/* musb CLKIN in MHZ */
};

static struct musb_hdrc_platform_data musb_plat = {
#if defined(CONFIG_USB_MUSB_OTG)
	.mode		= MUSB_OTG,
#elif defined(CONFIG_USB_MUSB_HDRC_HCD)
	.mode		= MUSB_HOST,
#elif defined(CONFIG_USB_GADGET_MUSB_HDRC)
	.mode		= MUSB_PERIPHERAL,
#endif
	.config		= &musb_config,
};

static u64 musb_dmamask = ~(u32)0;

static struct platform_device musb_device = {
	.name		= "musb-blackfin",
	.id		= 0,
	.dev = {
		.dma_mask		= &musb_dmamask,
		.coherent_dma_mask	= 0xffffffff,
		.platform_data		= &musb_plat,
	},
	.num_resources	= ARRAY_SIZE(musb_resources),
	.resource	= musb_resources,
};
#endif

#if defined(CONFIG_FB_BFIN_LQ035Q1) || defined(CONFIG_FB_BFIN_LQ035Q1_MODULE)
#include <asm/bfin-lq035q1.h>

static struct bfin_lq035q1fb_disp_info bfin_lq035q1_data = {
	.mode = LQ035_NORM | LQ035_RGB | LQ035_RL | LQ035_TB,
	.ppi_mode = USE_RGB565_16_BIT_PPI,
	//.use_bl = 1,
        .use_bl = 0,
	.gpio_bl = LCD_BACKLIGHT_GPIO,
	//.gpio_bl = 0,
};

static struct resource bfin_lq035q1_resources[] = {
	{
		.start = IRQ_PPI_ERROR,
		.end = IRQ_PPI_ERROR,
		.flags = IORESOURCE_IRQ,
	},
};

static struct platform_device bfin_lq035q1_device = {
	.name		= "bfin-lq035q1",
	.id		= -1,
	.num_resources	= ARRAY_SIZE(bfin_lq035q1_resources),
	.resource	= bfin_lq035q1_resources,
	.dev		= {
		.platform_data = &bfin_lq035q1_data,
	},
};
#endif

#if defined(CONFIG_GPIO_ADP5588) || defined(CONFIG_GPIO_ADP5588_MODULE)
static struct adp5588_gpio_platform_data adp5588_gpio_data = {
	.gpio_start = 0x64,
	.pullup_dis_mask = 0,
};
#endif

#if defined(CONFIG_INPUT_ADXL34X) || defined(CONFIG_INPUT_ADXL34X_MODULE)
#include <linux/input/adxl34x.h>
static const struct adxl34x_platform_data adxl345_info = {
	.x_axis_offset = 0,
	.y_axis_offset = 0,
	.z_axis_offset = 0,
	.tap_threshold = 0x31,
	.tap_duration = 0x10,
	.tap_latency = 0x60,
	.tap_window = 0xF0,
	.tap_axis_control = ADXL_TAP_X_EN | ADXL_TAP_Y_EN | ADXL_TAP_Z_EN,
	.act_axis_control = 0xFF,
	.activity_threshold = 5,
	.inactivity_threshold = 2,
	.inactivity_time = 2,
	.free_fall_threshold = 0x7,
	.free_fall_time = 0x20,
	.data_rate = 0x8,
	.data_range = ADXL_FULL_RES,

	.ev_type = EV_ABS,
	.ev_code_x = ABS_X,		/* EV_REL */
	.ev_code_y = ABS_Y,		/* EV_REL */
	.ev_code_z = ABS_Z,		/* EV_REL */

	.ev_code_tap = {BTN_TOUCH, BTN_TOUCH, BTN_TOUCH}, /* EV_KEY x,y,z */

/*	.ev_code_ff = KEY_F,*/		/* EV_KEY */
	.ev_code_act_inactivity = KEY_A,	/* EV_KEY */
	.use_int2 = 1,
	.power_mode = ADXL_AUTO_SLEEP | ADXL_LINK,
	.fifo_mode = ADXL_FIFO_STREAM,
};
#endif

#if defined(CONFIG_INPUT_AD714X_I2C) || defined(CONFIG_INPUT_AD714X_I2C_MODULE)
#include <linux/input/ad714x.h>

static struct ad714x_button_plat ad7147_button_plat[] = {
/*
Note: This maping has no relation to the physical mapping of
the buttons. The harware buttons are identified using CIN
where as the mapping here correspond to which stage's high
and low correspond to which Marco. For physical mappings see
the variable touch_dev_platform_data
*/
	{
	.keycode = BTN_BASE, //corresponds to PB1
	.l_mask = 0,
	.h_mask = 0x100, //mapped to +ve of stage8
	},
	{
	.keycode = BTN_BASE2, //corresponds to PB2
	.l_mask = 0,
	.h_mask = 0x200, //mapped to +ve of stage9
	},
	{
	.keycode = BTN_BASE3, //corresponds to PB3
	.l_mask = 0,
	.h_mask = 0x400, //mapped to +ve of stage10
	},
	{
	.keycode = BTN_BASE4, //corresponds to PB4
	.l_mask = 0x0,
	.h_mask = 0x800, //mapped to +ve of stage11
	},
	{
	.keycode = BTN_BASE5, //corresponds to PB5
	.l_mask = 0x100, //mapped to -ve of stage8
	.h_mask = 0x0,
	},
        {
	.keycode = BTN_0, //corresponds to WHEEL_0
	.l_mask = 0,
	.h_mask = 0x10, //mapped to +ve of stage4
	},
	{
	.keycode = BTN_1, //corresponds to WHEEL_1
	.l_mask = 0,
	.h_mask = 0x20, //mapped to +ve of stage5
	},
	{
	.keycode = BTN_2, //corresponds to WHEEL_2
	.l_mask = 0,
	.h_mask = 0x40, //mapped to +ve of stage6
	},
	{
	.keycode = BTN_3, //corresponds to WHEEL_3
	.l_mask = 0,
	.h_mask = 0x80, //mapped to +ve of stage7
	},
	{
	.keycode = BTN_4, //corresponds to WHEEL_4
	.l_mask = 0,
	.h_mask = 0x1, //mapped to +ve of stage0
	},
	{
	.keycode = BTN_5, //corresponds to WHEEL_5
	.l_mask = 0,
	.h_mask = 0x2, //mapped to +ve of stage1
	},
	{
	.keycode = BTN_6, //corresponds to WHEEL_6
	.l_mask = 0,
	.h_mask = 0x4, //mapped to +ve of stage2
	},
	{
	.keycode = BTN_7, //corresponds to WHEEL_7
	.l_mask = 0x0,
	.h_mask = 0x8,  //mapped to +ve of stage3
	},
};


static struct ad714x_wheel_plat ad7147_wheel_plat = {
.start_stage = 0,
.end_stage = 7,
.max_coord = 1024,
};

static struct ad714x_slider_plat ad7147_slider_plat = {
.start_stage = 0,
.end_stage = 7,
.max_coord = 128,
};

/* Comes from LCD EZ-Extender
static struct ad714x_platform_data touch_dev_platform_data = {
.wheel_num = 1,
.button_num = 4,
.slider_num = 0,
.slider = &ad7147_slider_plat,
.button = ad7147_button_plat,
.wheel = &ad7147_wheel_plat,
.stage_cfg_reg = {
{0xEFFF, 0x1FFF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFFFF, 0x1FFE, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFFFF, 0x1FFB, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFFFF, 0x1FEF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFFFF, 0x1FBF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFFFF, 0x1EFF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFEFF, 0x1FFF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFBFF, 0x1FFF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},

{0xFFFE, 0x1FFF, 0x0000, 0x2626, 5664, 5664, 7080, 7080},
{0xFFFB, 0x1FFF, 0x0000, 0x2626, 5664, 5664, 7080, 7080},
{0xFFEF, 0x1FFF, 0x0000, 0x2626, 5664, 5664, 7080, 7080},
{0xFFBF, 0x1FFF, 0x0000, 0x2626, 5664, 5664, 7080, 7080},
},
.sys_cfg_reg = {0x00B2, 0x0, 0x3230, 0x0819, 0x0832, 0x0FFF, 0x0FFF, 0x0000}, 

};
*/
/* For testing wheel
static struct ad714x_platform_data touch_dev_platform_data = {
.wheel_num = 1,
.button_num = 4,
.slider_num = 0,
.slider = &ad7147_slider_plat,
.button = ad7147_button_plat,
.wheel = &ad7147_wheel_plat,
.stage_cfg_reg = {
{0xFEFF, 0x1FFF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFBFF, 0x1FFF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xEFFF, 0x1FFF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFFFF, 0x1FFE, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFFFF, 0x1FFB, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFFFF, 0x1FEF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFFFF, 0x1FBF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
{0xFFFF, 0x1EFF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},

{0xFFFE, 0x1FFF, 0x0000, 0x2626, 5664, 5664, 7080, 7080},
{0xFFFB, 0x1FFF, 0x0000, 0x2626, 5664, 5664, 7080, 7080},
{0xFFEF, 0x1FFF, 0x0000, 0x2626, 5664, 5664, 7080, 7080},
{0xFFBF, 0x1FFF, 0x0000, 0x2626, 5664, 5664, 7080, 7080},
},
.sys_cfg_reg = {0x00B2, 0x0, 0x3230, 0x0819, 0x0832, 0x0FFF, 0x0FFF, 0x0000}, 

};
*/

static struct ad714x_platform_data touch_dev_platform_data = {
.wheel_num = 1, // since we are using wheel
.button_num = 13, //since all 13 buttons(PB1-PB5 & 8 wheel buttons) are used
.slider_num = 0, // since we are not using slider
//.slider = &ad7147_slider_plat,
.button = ad7147_button_plat,
.wheel = &ad7147_wheel_plat,
.stage_cfg_reg = {
/*
The physical CIN mappings are:
CIN0  -  PB1
CIN1  -  PB2
CIN2  -  PB3
CIN3  -  PB4
CIN4  -  WHEEL_0
CIN5  -  WHEEL_1
CIN6  -  WHEEL_2
CIN7  -  WHEEL_3
CIN8  -  WHEEL_4
CIN9  -  WHEEL_5
CIN10 -  WHEEL_6
CIN11 -  WHEEL_7
CIN12 -  PB5

Each row corresponds to the settings done to each stage from 0 through 11.
The different elements of a row are explained as follows:
1st & 2nd element:
 - 00 specifies CIN not connected to the stage
 - 01 specifies CIN connected to -ve of the stage
 - 10 specifies CIN connected to +ve of the stage
 - 11 specifies CIN connected to BIAS.(connect non connected CIN to the stage)
Notes :even though 00 makes sense for not connected, most of the examples 
use 11 for not connected CIN
 - The 1st element takes care from CIN 0 through CIN 6 using 14 bits 
of it such that 0th & 1st bit corresponds to CIN 0 and 12th &13th bit 
correspond to CIN 6. 14th & 15th bit are unused
 - Likewise for the 2nd element, first 12 bits correspond to CIN 7
through CIN 12 such that 0th & 1st bit corresponds to CIN 7 and 10th &11th bit 
correspond to CIN 12. Bit 13th & 14th correspond to Stage connection setup.
   00  - Do not use (The stage is disabled)
   01  - One CIN connected to +ve
   10  - One CIN connected to -ve
   11  - Differential (One CIN connected to +ve and another to -ve)
3rd Element:
 - Stage AFE offset for the stage (refer datasheet)
4th Element:
 - Sensitivity for the stage (refer datasheet)
5th Element:
 - Stage offset low value (refer datasheet)
6th Element:
 - Stage offset high value (refer datasheet)
7th Element:
 - Stage offset high clamp value (refer datasheet)
8th Element:
 - Stage offset low clamp value (refer datasheet)
 
*/
//Setting for wheel
{0x3EFF, 0x1FFF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
//Stage 0 - CIN4(Wheel_0) connected to +ve
{0x3BFF, 0x1FFF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
//Stage 1 - CIN5(Wheel_2) connected to +ve
{0x2FFF, 0x1FFF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
//Stage 2 - CIN6(Wheel_3) connected to +ve
{0x3FFF, 0x1FFE, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
//Stage 3 - CIN7(Wheel_4) connected to +ve
{0x3FFF, 0x1FFB, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
//Stage 4 - CIN8(Wheel_5) connected to +ve
{0x3FFF, 0x1FEF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
//Stage 5 - CIN9(Wheel_6) connected to +ve
{0x3FFF, 0x1FBF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
//Stage 6 - CIN10(Wheel_7) connected to +ve
{0x3FFF, 0x1EFF, 0x0000, 0x2626, 2920, 2920, 3650, 3650},
//Stage 7 - CIN11(Wheel_8) connected to +ve

//Setting for PB1 to PB5
{0xFFFE, 0x37FF, 0x0000, 0x2626, 5664, 5664, 7080, 7080},
//Stage 8 - CIN0(PB1) connected to +ve
//        - CIN12(PB5) connected to -ve
{0xFFFB, 0x1FFF, 0x0000, 0x2626, 5664, 5664, 7080, 7080},
//Stage 9 - CIN1(PB2) connected to +ve
{0xFFEF, 0x1FFF, 0x0000, 0x2626, 5664, 5664, 7080, 7080},
//Stage 10 - CIN2(PB3) connected to +ve
{0xFFBF, 0x1FFF, 0x0000, 0x2626, 5664, 5664, 7080, 7080},
//Stage 11 - CIN3(PB4) connected to +ve
},
.sys_cfg_reg = {0x00B2, 0x0, 0x3230, 0x0819, 0x0832, 0x0FFF, 0x0FFF, 0x0000}, 

};

#endif

#if defined(CONFIG_RTC_DRV_BFIN) || defined(CONFIG_RTC_DRV_BFIN_MODULE)
static struct platform_device rtc_device = {
	.name = "rtc-bfin",
	.id   = -1,
};
#endif

#if defined(CONFIG_BFIN_MAC) || defined(CONFIG_BFIN_MAC_MODULE)
#include <linux/bfin_mac.h>
static const unsigned short bfin_mac_peripherals[] = P_RMII0;

static struct bfin_phydev_platform_data bfin_phydev_data[] = {
	{
		.addr = 1,
		.irq = IRQ_MAC_PHYINT,
	},
};

static struct bfin_mii_bus_platform_data bfin_mii_bus_data = {
	.phydev_number = 1,
	.phydev_data = bfin_phydev_data,
	.phy_mode = PHY_INTERFACE_MODE_RMII,
	.mac_peripherals = bfin_mac_peripherals,
};

static struct platform_device bfin_mii_bus = {
	.name = "bfin_mii_bus",
	.dev = {
		.platform_data = &bfin_mii_bus_data,
	}
};

static struct platform_device bfin_mac_device = {
	.name = "bfin_mac",
	.dev = {
		.platform_data = &bfin_mii_bus,
	}
};
#endif

#if defined(CONFIG_MTD_M25P80) \
	|| defined(CONFIG_MTD_M25P80_MODULE)

	/* 
	 * SPI flash chip (m25p80 driver: type = s25sl12800 - Spansion) 
	 * Flash memory on board TLL6527Mv1-1 is Spansion S25FL12801
	 * 256 KB unform size / sector X 64 sectors = 16 MB
	 */

static struct mtd_partition bfin_spi_flash_partitions[] = {
 {
	/*
	 * Reserved 512 KB, (sectors 0-1) for u-boot with environment.
	 * Preferred to keep environment on sector boundary, Read only
	 * from linux.
	 */
	.name = "bootloader(spi)",
	.size = 0x00080000,
	.offset = 0,
	.mask_flags = MTD_WRITEABLE
        /*
         * The mask_flag is to ignore the given flags. 
         * http://blackfin.uclinux.org/gf/project/uclinux-dist/forum/?_forum_a
         * ction=ForumMessageBrowse&thread_id=34588&action=ForumBrowse
         * The above link pointing to the forum post which explains what is 
         * mask_flag. 
         * According to the forum TO MAKE A PATRITION READONLY we NEED to 
         * specify the mask_flag as MTD_WRITEABLE
         * 
        */
 }, {
	/*
	 * Reserved 1 MB (sectors 2-5) for fpga binary - 277 KB for 
	 * TLL6527Mv1-1s Spartan 3E XC3S500E FPGA, alternate FPGA
         * and load program binary (approx 90KB) Maintaining sector
         * boundary 
	 */
	 .name = "fpga binary (spi)",
	 .size = 0x00100000,
	 .offset = MTDPART_OFS_APPEND,
	 .mask_flags = MTD_WRITEABLE
 }, {
	/*
	 * Reserved 8 MB (sectors 6-37) for uClinux image (kernel + ramfs)
	 */
	 .name = "linux kernel(spi)",
	 .size = 0x800000,
	 .offset = MTDPART_OFS_APPEND,
	 .mask_flags = MTD_WRITEABLE
 }, {
	/*
	 * Reserved 2.5 MB (sectors 38-47) Free space/User space
	 */
	 .name = "free space(spi)",
	 .size = 0x280000,
	 .offset = MTDPART_OFS_APPEND,
	 .mask_flags = MTD_WRITEABLE
 }, {
	/*
	 * Reserved rest of the 4MB space (sectors 48 - 63) for user space
	 * accessible jffs2 file system
	 */
	 .name = "file system(spi)",
	 .size = MTDPART_SIZ_FULL,
	 .offset = MTDPART_OFS_APPEND,
 }
};

static struct flash_platform_data bfin_spi_flash_data = {
	 .name = "m25p80",
 	 .parts = bfin_spi_flash_partitions,
	 .nr_parts = ARRAY_SIZE(bfin_spi_flash_partitions),
	 .type = "s25sl12800",
	 /* defined in /drivers/mtd/devices/m25p80.c */
};

static struct bfin5xx_spi_chip spi_flash_chip_info = {
 .enable_dma = 0,         /* use dma transfer with this chip*/
 //.bits_per_word = 8,
 //The new struct bfin5xx_spi_chip doesnot have .bits_per_word
};
#endif


#if defined(CONFIG_MMC_SPI) || defined(CONFIG_MMC_SPI_MODULE)
static struct bfin5xx_spi_chip  mmc_spi_chip_info = {
	.enable_dma = 0,
	//.bits_per_word = 8,
//The new struct bfin5xx_spi_chip doesnot have .bits_per_word
};
#endif

#if defined(CONFIG_TOUCHSCREEN_AD7879) \
	|| defined(CONFIG_TOUCHSCREEN_AD7879_MODULE)
static const struct ad7879_platform_data bfin_ad7879_ts_info = {
	.model			= 7879,	/* Model = AD7879 */
	.x_plate_ohms		= 620,	/* 620 Ohm from the touch datasheet */
	.pressure_max		= 10000,
	.pressure_min		= 0,
	.first_conversion_delay = 3,
				/* wait 512us before do a first conversion */
	.acquisition_time	= 1,	/* 4us acquisition time per sample */
	.median			= 2,	/* do 8 measurements */
	.averaging		= 1,
				/* take the average of 4 middle samples */
	.pen_down_acc_interval	= 255,	/* 9.4 ms */
	.gpio_export		= 1,	/* configure AUX as GPIO output*/
	.gpio_base		= LCD_BACKLIGHT_GPIO,
};
#endif

#if defined(CONFIG_IIO_SYSFS_TRIGGER) || \
	defined(CONFIG_IIO_SYSFS_TRIGGER_MODULE)
static struct platform_device iio_sysfs_trigger = {
	.name		= "iio_sysfs_trigger",
	.id		= 0,
	.dev = {
		.platform_data = NULL,
	},
};
#endif

#if defined(CONFIG_IIO_BFIN_TMR_TRIGGER) || \
        defined(CONFIG_IIO_BFIN_TMR_TRIGGER_MODULE)
static struct resource iio_bfin_trigger_resources[] = {
	{
		.start = IRQ_TIMER3,
		.end = IRQ_TIMER3,
		.flags = IORESOURCE_IRQ,
	},
};
 
static struct platform_device iio_bfin_trigger = {
	.name		= "iio_bfin_tmr_trigger",
	.id		= 0,
	.num_resources	= ARRAY_SIZE(iio_bfin_trigger_resources),
	.resource	= iio_bfin_trigger_resources,
};
#endif
/*
#if defined(CONFIG_SPI_SPIDEV) || defined(CONFIG_SPI_SPIDEV_MODULE)
static struct bfin5xx_spi_chip spidev_chip_info = {
	.enable_dma = 0,
//	.bits_per_word = 8,
//removed from structure bfin5xx_spi_chip
};
#endif
*/
//Added the new change for CONFIG_SND_BF5XXX_I2S/TDM/AC97
#if defined(CONFIG_SND_BF5XX_I2S) || defined(CONFIG_SND_BF5XX_I2S_MODULE) || \
	defined(CONFIG_SND_BF5XX_TDM) || defined(CONFIG_SND_BF5XX_TDM_MODULE)

static const u16 bfin_snd_pin[][7] = {
	{P_SPORT0_DTPRI, P_SPORT0_TSCLK, P_SPORT0_RFS,
		P_SPORT0_DRPRI, P_SPORT0_RSCLK, 0, 0},
	{P_SPORT1_DTPRI, P_SPORT1_TSCLK, P_SPORT1_RFS,
		P_SPORT1_DRPRI, P_SPORT1_RSCLK, P_SPORT1_TFS, 0},
};

static struct bfin_snd_platform_data bfin_snd_data[] = {
	{
		.pin_req = &bfin_snd_pin[0][0],
	},
	{
		.pin_req = &bfin_snd_pin[1][0],
	},
};

#define BFIN_SND_RES(x) \
	[x] = { \
		{ \
			.start = SPORT##x##_TCR1, \
			.end = SPORT##x##_TCR1, \
			.flags = IORESOURCE_MEM \
		}, \
		{ \
			.start = CH_SPORT##x##_RX, \
			.end = CH_SPORT##x##_RX, \
			.flags = IORESOURCE_DMA, \
		}, \
		{ \
			.start = CH_SPORT##x##_TX, \
			.end = CH_SPORT##x##_TX, \
			.flags = IORESOURCE_DMA, \
		}, \
		{ \
			.start = IRQ_SPORT##x##_ERROR, \
			.end = IRQ_SPORT##x##_ERROR, \
			.flags = IORESOURCE_IRQ, \
		} \
	}

static struct resource bfin_snd_resources[][4] = {
	BFIN_SND_RES(0),
	BFIN_SND_RES(1),
};
#endif

#if defined(CONFIG_SND_BF5XX_I2S) || defined(CONFIG_SND_BF5XX_I2S_MODULE)
static struct platform_device bfin_i2s_pcm = {
	.name = "bfin-i2s-pcm-audio",
	.id = -1,
};
#endif

#if defined(CONFIG_SND_BF5XX_TDM) || defined(CONFIG_SND_BF5XX_TDM_MODULE)
static struct platform_device bfin_tdm_pcm = {
	.name = "bfin-tdm-pcm-audio",
	.id = -1,
};
#endif

#if defined(CONFIG_SND_BF5XX_AC97) || defined(CONFIG_SND_BF5XX_AC97_MODULE)
static struct platform_device bfin_ac97_pcm = {
	.name = "bfin-ac97-pcm-audio",
	.id = -1,
};
#endif

#if defined(CONFIG_SND_BF5XX_I2S) || defined(CONFIG_SND_BF5XX_I2S_MODULE)
static struct platform_device bfin_i2s = {
	.name = "bfin-i2s",
	.id = CONFIG_SND_BF5XX_SPORT_NUM,
//Platform data -Start
	.num_resources = ARRAY_SIZE(bfin_snd_resources[CONFIG_SND_BF5XX_SPORT_NUM]),
	.resource = bfin_snd_resources[CONFIG_SND_BF5XX_SPORT_NUM],
	.dev = {
		.platform_data = &bfin_snd_data[CONFIG_SND_BF5XX_SPORT_NUM],
	},
//platform data -end
};
#endif

#if defined(CONFIG_SND_BF5XX_TDM) || defined(CONFIG_SND_BF5XX_TDM_MODULE)
static struct platform_device bfin_tdm = {
	.name = "bfin-tdm",
	.id = CONFIG_SND_BF5XX_SPORT_NUM,
	.num_resources = ARRAY_SIZE(bfin_snd_resources[CONFIG_SND_BF5XX_SPORT_NUM]),
	.resource = bfin_snd_resources[CONFIG_SND_BF5XX_SPORT_NUM],
	.dev = {
		.platform_data = &bfin_snd_data[CONFIG_SND_BF5XX_SPORT_NUM],
	},
};
#endif

//Added the new change for CONFIG_SND_BF5XXX_I2S/TDM/AC97 - end

#if defined(CONFIG_FB_BFIN_LQ035Q1) || defined(CONFIG_FB_BFIN_LQ035Q1_MODULE)
static struct bfin5xx_spi_chip lq035q1_spi_chip_info = {
	.enable_dma	= 0,
//	.bits_per_word	= 8,
//The new struct bfin5xx_spi_chip doesnot have .bits_per_word
};
#endif




static struct spi_board_info bfin_spi_board_info[] __initdata = {
#if defined(CONFIG_MTD_M25P80) \
	|| defined(CONFIG_MTD_M25P80_MODULE)

/*
 * TLL6527Mv1-1 has an SPI Chip Select line mapped to the GPIO connector.
 * User can add external devices to that line. It is PH9/nSPISEL5_3V3.
 * Its native bfin SPI slave select #5. For specific SPI MODES, it can be
 * used to automatically toggle CS per word transfer
 */
	{
		/* the modalias must be the same as spi device driver name */
		.modalias = "m25p80", /* Name of spi_driver for this device */
		.max_speed_hz = 20000000,
				/* max spi clock (SCK) speed in HZ */
		.bus_num = 0, /* Framework bus number */
		.chip_select = GPIO_PG1 + MAX_CTRL_CS,
		.platform_data = &bfin_spi_flash_data,
		.controller_data = &spi_flash_chip_info,
		.mode = SPI_MODE_3,
	},
#endif

#if defined(CONFIG_MMC_SPI) || defined(CONFIG_MMC_SPI_MODULE)
	{
		.modalias = "mmc_spi",
/*
 * TLL6527M V1.0 does not support SD Card at SPI Clock > 10 MHz due to
 * SPI buffer limitations
 */
		.max_speed_hz = 20000000,
		//.max_speed_hz = 5000000,
					/* max spi clock (SCK) speed in HZ */
		.bus_num = 0,
		.chip_select = GPIO_PH12 + MAX_CTRL_CS,
		.controller_data = &mmc_spi_chip_info,
		.mode = SPI_MODE_0,
	},
#endif
/*
#if defined(CONFIG_SPI_SPIDEV) || defined(CONFIG_SPI_SPIDEV_MODULE)
	{
		.modalias = "spidev",
		.max_speed_hz = 20000000,*/
		/* TLL6527Mv1-0 supports max spi clock (SCK) speed = 10 MHz *//*
		.bus_num = 0,
		.chip_select = GPIO_PH9 + MAX_CTRL_CS,
		.mode = SPI_MODE_3,
		.controller_data = &spidev_chip_info,
	},
#endif
*/
#if defined(CONFIG_FB_BFIN_LQ035Q1) || defined(CONFIG_FB_BFIN_LQ035Q1_MODULE)
	{
		.modalias = "bfin-lq035q1-spi",
		.max_speed_hz = 15000000,
		.bus_num = 0,
		/* TLL6527Mv1-1 has PH10 mapped to LCD CS */
		.chip_select = GPIO_PH10 + MAX_CTRL_CS,
		.controller_data = &lq035q1_spi_chip_info,
		.mode = SPI_CPHA | SPI_CPOL,
	},
#endif

};

#if defined(CONFIG_SPI_BFIN5XX) || defined(CONFIG_SPI_BFIN5XX_MODULE)
/* SPI controller data */
static struct bfin5xx_spi_master bfin_spi0_info = {
	/* Check value of MAX_BLACKFIN_GPIOS & replace 8 with it  */
	.num_chipselect = 48 + MAX_CTRL_CS,
	.enable_dma = 1,  /* master has the ability to do dma transfer */
	.pin_req = {P_SPI0_SCK, P_SPI0_MISO, P_SPI0_MOSI, 0},
};

/* SPI (0) */
static struct resource bfin_spi0_resource[] = {
	[0] = {
		.start = SPI0_REGBASE,
		.end   = SPI0_REGBASE + 0xFF,
		.flags = IORESOURCE_MEM,
		},
	[1] = {
		.start = CH_SPI,
		.end   = CH_SPI,
		.flags = IORESOURCE_DMA,
	},
	[2] = {
		.start = IRQ_SPI,
		.end   = IRQ_SPI,
		.flags = IORESOURCE_IRQ,
	},
};

static struct platform_device bfin_spi0_device = {
	.name = "bfin-spi",
	.id = 0, /* Bus number */
	.num_resources = ARRAY_SIZE(bfin_spi0_resource),
	.resource = bfin_spi0_resource,
	.dev = {
		.platform_data = &bfin_spi0_info, /* Passed to driver */
	},
};
#endif  /* spi master and devices */

#if defined(CONFIG_SERIAL_BFIN) || defined(CONFIG_SERIAL_BFIN_MODULE)
#ifdef CONFIG_SERIAL_BFIN_UART0
static struct resource bfin_uart0_resources[] = {
	{
		.start = UART0_THR,
		.end = UART0_GCTL+2,
		.flags = IORESOURCE_MEM,
	},
	{
		.start = IRQ_UART0_TX,
		.end = IRQ_UART0_TX,
		.flags = IORESOURCE_IRQ,
	},
	{
		.start = IRQ_UART0_RX,
		.end = IRQ_UART0_RX,
		.flags = IORESOURCE_IRQ,
	},
	{
		.start = IRQ_UART0_ERROR,
		.end = IRQ_UART0_ERROR,
		.flags = IORESOURCE_IRQ,
	},
	{
		.start = CH_UART0_TX,
		.end = CH_UART0_TX,
		.flags = IORESOURCE_DMA,
	},
	{
		.start = CH_UART0_RX,
		.end = CH_UART0_RX,
		.flags = IORESOURCE_DMA,
	},
};

static unsigned short bfin_uart0_peripherals[] = {
	P_UART0_TX, P_UART0_RX, 0
};

static struct platform_device bfin_uart0_device = {
	.name = "bfin-uart",
	.id = 0,
	.num_resources = ARRAY_SIZE(bfin_uart0_resources),
	.resource = bfin_uart0_resources,
	.dev = {
		.platform_data = &bfin_uart0_peripherals,
					/* Passed to driver */
	},
};
#endif
#ifdef CONFIG_SERIAL_BFIN_UART1
static struct resource bfin_uart1_resources[] = {
	{
		.start = UART1_THR,
		.end = UART1_GCTL+2,
		.flags = IORESOURCE_MEM,
	},
	{
		.start = IRQ_UART1_TX,
		.end = IRQ_UART1_TX,
		.flags = IORESOURCE_IRQ,
	},
	{
		.start = IRQ_UART1_RX,
		.end = IRQ_UART1_RX,
		.flags = IORESOURCE_IRQ,
	},
	{
		.start = IRQ_UART1_ERROR,
		.end = IRQ_UART1_ERROR,
		.flags = IORESOURCE_IRQ,
	},
	{
		.start = CH_UART1_TX,
		.end = CH_UART1_TX,
		.flags = IORESOURCE_DMA,
	},
	{
		.start = CH_UART1_RX,
		.end = CH_UART1_RX,
		.flags = IORESOURCE_DMA,
	},
#ifdef CONFIG_BFIN_UART1_CTSRTS
	{	/* CTS pin */
		.start = GPIO_PF9,
		.end = GPIO_PF9,
		.flags = IORESOURCE_IO,
	},
	{	/* RTS pin */
		.start = GPIO_PF10,
		.end = GPIO_PF10,
		.flags = IORESOURCE_IO,
	},
#endif
};

static unsigned short bfin_uart1_peripherals[] = {
	P_UART1_TX, P_UART1_RX, 0
};

static struct platform_device bfin_uart1_device = {
	.name = "bfin-uart",
	.id = 1,
	.num_resources = ARRAY_SIZE(bfin_uart1_resources),
	.resource = bfin_uart1_resources,
	.dev = {
		.platform_data = &bfin_uart1_peripherals,
						/* Passed to driver */
	},
};
#endif
#endif

#if defined(CONFIG_BFIN_SIR) || defined(CONFIG_BFIN_SIR_MODULE)
#ifdef CONFIG_BFIN_SIR0
static struct resource bfin_sir0_resources[] = {
	{
		.start = 0xFFC00400,
		.end = 0xFFC004FF,
		.flags = IORESOURCE_MEM,
	},
	{
		.start = IRQ_UART0_RX,
		.end = IRQ_UART0_RX+1,
		.flags = IORESOURCE_IRQ,
	},
	{
		.start = CH_UART0_RX,
		.end = CH_UART0_RX+1,
		.flags = IORESOURCE_DMA,
	},
};

static struct platform_device bfin_sir0_device = {
	.name = "bfin_sir",
	.id = 0,
	.num_resources = ARRAY_SIZE(bfin_sir0_resources),
	.resource = bfin_sir0_resources,
};
#endif
#ifdef CONFIG_BFIN_SIR1
static struct resource bfin_sir1_resources[] = {
	{
		.start = 0xFFC02000,
		.end = 0xFFC020FF,
		.flags = IORESOURCE_MEM,
	},
	{
		.start = IRQ_UART1_RX,
		.end = IRQ_UART1_RX+1,
		.flags = IORESOURCE_IRQ,
	},
	{
		.start = CH_UART1_RX,
		.end = CH_UART1_RX+1,
		.flags = IORESOURCE_DMA,
	},
};

static struct platform_device bfin_sir1_device = {
	.name = "bfin_sir",
	.id = 1,
	.num_resources = ARRAY_SIZE(bfin_sir1_resources),
	.resource = bfin_sir1_resources,
};
#endif
#endif

#if defined(CONFIG_I2C_BLACKFIN_TWI) || defined(CONFIG_I2C_BLACKFIN_TWI_MODULE)
static struct resource bfin_twi0_resource[] = {
	[0] = {
		.start = TWI0_REGBASE,
		.end   = TWI0_REGBASE,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_TWI,
		.end   = IRQ_TWI,
		.flags = IORESOURCE_IRQ,
	},
};

static struct platform_device i2c_bfin_twi_device = {
	.name = "i2c-bfin-twi",
	.id = 0,
	.num_resources = ARRAY_SIZE(bfin_twi0_resource),
	.resource = bfin_twi0_resource,
};
#endif

static struct i2c_board_info __initdata bfin_i2c_board_info[] = {
#if defined(CONFIG_BFIN_TWI_LCD) || defined(CONFIG_BFIN_TWI_LCD_MODULE)
	{
		I2C_BOARD_INFO("pcf8574_lcd", 0x22),
	},
#endif

#if defined(CONFIG_FB_BFIN_7393) || defined(CONFIG_FB_BFIN_7393_MODULE)
	{
		I2C_BOARD_INFO("bfin-adv7393", 0x2B),
	},
#endif
#if defined(CONFIG_TOUCHSCREEN_AD7879_I2C) \
	|| defined(CONFIG_TOUCHSCREEN_AD7879_I2C_MODULE)
	{
		I2C_BOARD_INFO("ad7879", 0x2F),
		.irq = IRQ_PH14,
		.platform_data = (void *)&bfin_ad7879_ts_info,
	},
#endif
#if defined(CONFIG_SND_SOC_SSM2602) || defined(CONFIG_SND_SOC_SSM2602_MODULE)
	{
		I2C_BOARD_INFO("ssm2602", 0x1b),
	},
#endif
	{
		I2C_BOARD_INFO("adm1192", 0x2e),
	},

	{
		I2C_BOARD_INFO("ltc3576", 0x09),
	},
#if defined(CONFIG_INPUT_ADXL34X_I2C) \
	|| defined(CONFIG_INPUT_ADXL34X_I2C_MODULE)
	{
		I2C_BOARD_INFO("adxl34x", 0x53),
		.irq = IRQ_PH13,
		.platform_data = (void *)&adxl345_info,
	},
#endif

#if defined(CONFIG_INPUT_AD714X_I2C) || defined(CONFIG_INPUT_AD714X_I2C_MODULE)
	{
		I2C_BOARD_INFO("ad7147_captouch", 0x2C),
		.irq = IRQ_PH15,
		.platform_data = (void *)&touch_dev_platform_data,
	},
#endif

#if defined(CONFIG_GPIO_ADP5588) || defined(CONFIG_GPIO_ADP5588_MODULE)
	{
		I2C_BOARD_INFO("adp5588-gpio", 0x34),
		.platform_data = (void *)&adp5588_gpio_data,
	},
#endif
#if defined(CONFIG_SENSORS_LM75) \
	|| defined(CONFIG_SENSORS_LM75_MODULE)
	{
		I2C_BOARD_INFO("lm75", 0x48),
	},
#endif
};

#if defined(CONFIG_SERIAL_BFIN_SPORT) \
	|| defined(CONFIG_SERIAL_BFIN_SPORT_MODULE)
#ifdef CONFIG_SERIAL_BFIN_SPORT0_UART
static struct resource bfin_sport0_uart_resources[] = {
	{
		.start = SPORT0_TCR1,
		.end = SPORT0_MRCS3+4,
		.flags = IORESOURCE_MEM,
	},
	{
		.start = IRQ_SPORT0_RX,
		.end = IRQ_SPORT0_RX+1,
		.flags = IORESOURCE_IRQ,
	},
	{
		.start = IRQ_SPORT0_ERROR,
		.end = IRQ_SPORT0_ERROR,
		.flags = IORESOURCE_IRQ,
	},
};

static unsigned short bfin_sport0_peripherals[] = {
	P_SPORT0_TFS, P_SPORT0_DTPRI, P_SPORT0_TSCLK, P_SPORT0_RFS,
	P_SPORT0_DRPRI, P_SPORT0_RSCLK, 0
};

static struct platform_device bfin_sport0_uart_device = {
	.name = "bfin-sport-uart",
	.id = 0,
	.num_resources = ARRAY_SIZE(bfin_sport0_uart_resources),
	.resource = bfin_sport0_uart_resources,
	.dev = {
		.platform_data = &bfin_sport0_peripherals,
		/* Passed to driver */
	},
};
#endif
#ifdef CONFIG_SERIAL_BFIN_SPORT1_UART
static struct resource bfin_sport1_uart_resources[] = {
	{
		.start = SPORT1_TCR1,
		.end = SPORT1_MRCS3+4,
		.flags = IORESOURCE_MEM,
	},
	{
		.start = IRQ_SPORT1_RX,
		.end = IRQ_SPORT1_RX+1,
		.flags = IORESOURCE_IRQ,
	},
	{
		.start = IRQ_SPORT1_ERROR,
		.end = IRQ_SPORT1_ERROR,
		.flags = IORESOURCE_IRQ,
	},
};

static unsigned short bfin_sport1_peripherals[] = {
	P_SPORT1_TFS, P_SPORT1_DTPRI, P_SPORT1_TSCLK, P_SPORT1_RFS,
	P_SPORT1_DRPRI, P_SPORT1_RSCLK, 0
};

static struct platform_device bfin_sport1_uart_device = {
	.name = "bfin-sport-uart",
	.id = 1,
	.num_resources = ARRAY_SIZE(bfin_sport1_uart_resources),
	.resource = bfin_sport1_uart_resources,
	.dev = {
		.platform_data = &bfin_sport1_peripherals,
		/* Passed to driver */
	},
};
#endif
#endif

static const unsigned int cclk_vlev_datasheet[] = {
	VRPAIR(VLEV_100, 400000000),
	VRPAIR(VLEV_105, 426000000),
	VRPAIR(VLEV_110, 500000000),
	VRPAIR(VLEV_115, 533000000),
	VRPAIR(VLEV_120, 600000000),
};

static struct bfin_dpmc_platform_data bfin_dmpc_vreg_data = {
	.tuple_tab = cclk_vlev_datasheet,
	.tabsize = ARRAY_SIZE(cclk_vlev_datasheet),
	.vr_settling_time = 25 /* us */,
};

static struct platform_device bfin_dpmc = {
	.name = "bfin dpmc",
	.dev = {
		.platform_data = &bfin_dmpc_vreg_data,
	},
};

static struct platform_device *tll6527m_devices[] __initdata = {

	&bfin_dpmc,

#if defined(CONFIG_RTC_DRV_BFIN) || defined(CONFIG_RTC_DRV_BFIN_MODULE)
	&rtc_device,
#endif

#if defined(CONFIG_USB_MUSB_HDRC) || defined(CONFIG_USB_MUSB_HDRC_MODULE)
	&musb_device,
#endif

#if defined(CONFIG_BFIN_MAC) || defined(CONFIG_BFIN_MAC_MODULE)
	&bfin_mii_bus,
	&bfin_mac_device,
#endif

#if defined(CONFIG_SPI_BFIN5XX) || defined(CONFIG_SPI_BFIN5XX_MODULE)
	&bfin_spi0_device,
#endif

#if defined(CONFIG_FB_BFIN_LQ035Q1) || defined(CONFIG_FB_BFIN_LQ035Q1_MODULE)
	&bfin_lq035q1_device,
#endif

#if defined(CONFIG_SERIAL_BFIN) || defined(CONFIG_SERIAL_BFIN_MODULE)
#ifdef CONFIG_SERIAL_BFIN_UART0
	&bfin_uart0_device,
#endif
#ifdef CONFIG_SERIAL_BFIN_UART1
	&bfin_uart1_device,
#endif
#endif

#if defined(CONFIG_BFIN_SIR) || defined(CONFIG_BFIN_SIR_MODULE)
#ifdef CONFIG_BFIN_SIR0
	&bfin_sir0_device,
#endif
#ifdef CONFIG_BFIN_SIR1
	&bfin_sir1_device,
#endif
#endif

#if defined(CONFIG_I2C_BLACKFIN_TWI) || defined(CONFIG_I2C_BLACKFIN_TWI_MODULE)
	&i2c_bfin_twi_device,
#endif

#if defined(CONFIG_SERIAL_BFIN_SPORT) \
	|| defined(CONFIG_SERIAL_BFIN_SPORT_MODULE)
#ifdef CONFIG_SERIAL_BFIN_SPORT0_UART
	&bfin_sport0_uart_device,
#endif
#ifdef CONFIG_SERIAL_BFIN_SPORT1_UART
	&bfin_sport1_uart_device,
#endif
#endif

#if defined(CONFIG_MTD_GPIO_ADDR) || defined(CONFIG_MTD_GPIO_ADDR_MODULE)
	&tll6527m_flash_device,
#endif

#if defined(CONFIG_SND_BF5XX_I2S) || defined(CONFIG_SND_BF5XX_I2S_MODULE)
	&bfin_i2s_pcm,
#endif

#if defined(CONFIG_SND_BF5XX_TDM) || defined(CONFIG_SND_BF5XX_TDM_MODULE)
	&bfin_tdm_pcm,
#endif

#if defined(CONFIG_SND_BF5XX_AC97) || defined(CONFIG_SND_BF5XX_AC97_MODULE)
	&bfin_ac97_pcm,
#endif

#if defined(CONFIG_SND_BF5XX_I2S) || defined(CONFIG_SND_BF5XX_I2S_MODULE)
	&bfin_i2s,
#endif

#if defined(CONFIG_SND_BF5XX_TDM) || defined(CONFIG_SND_BF5XX_TDM_MODULE)
	&bfin_tdm,
#endif
#if defined(CONFIG_IIO_SYSFS_TRIGGER) || \
        defined(CONFIG_IIO_SYSFS_TRIGGER_MODULE)
	&iio_sysfs_trigger,
#endif
#if defined(CONFIG_IIO_BFIN_TMR_TRIGGER) || \
	defined(CONFIG_IIO_BFIN_TMR_TRIGGER_MODULE)
	&iio_bfin_trigger,
#endif
};

static int __init tll6527m_init(void)
{
	printk(KERN_INFO "%s(): registering device resources\n", __func__);
	i2c_register_board_info(0, bfin_i2c_board_info,
				ARRAY_SIZE(bfin_i2c_board_info));
	platform_add_devices(tll6527m_devices, ARRAY_SIZE(tll6527m_devices));
	spi_register_board_info(bfin_spi_board_info,
				ARRAY_SIZE(bfin_spi_board_info));
	return 0;
}

arch_initcall(tll6527m_init);

static struct platform_device *tll6527m_early_devices[] __initdata = {
#if defined(CONFIG_SERIAL_BFIN_CONSOLE) || defined(CONFIG_EARLY_PRINTK)
#ifdef CONFIG_SERIAL_BFIN_UART0
	&bfin_uart0_device,
#endif
#ifdef CONFIG_SERIAL_BFIN_UART1
	&bfin_uart1_device,
#endif
#endif

#if defined(CONFIG_SERIAL_BFIN_SPORT_CONSOLE)
#ifdef CONFIG_SERIAL_BFIN_SPORT0_UART
	&bfin_sport0_uart_device,
#endif
#ifdef CONFIG_SERIAL_BFIN_SPORT1_UART
	&bfin_sport1_uart_device,
#endif
#endif
};

void __init native_machine_early_platform_add_devices(void)
{
	printk(KERN_INFO "register early platform devices\n");
	early_platform_add_devices(tll6527m_early_devices,
		ARRAY_SIZE(tll6527m_early_devices));
}

void native_machine_restart(char *cmd)
{
	/* workaround reboot hang when booting from SPI */
	if ((bfin_read_SYSCR() & 0x7) == 0x3)
		bfin_reset_boot_spi_cs(P_DEFAULT_BOOT_SPI_CS);
}

void bfin_get_ether_addr(char *addr)
{
	/* the MAC is stored in OTP memory page 0xDF */
	u32 ret;
	u64 otp_mac;
	u32 (*otp_read)(u32 page, u32 flags,
			u64 *page_content) = (void *)0xEF00001A;

	ret = otp_read(0xDF, 0x00, &otp_mac);
	if (!(ret & 0x1)) {
		char *otp_mac_p = (char *)&otp_mac;
		for (ret = 0; ret < 6; ++ret)
			addr[ret] = otp_mac_p[5 - ret];
	}
}
EXPORT_SYMBOL(bfin_get_ether_addr);
